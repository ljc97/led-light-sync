# LED Light Sync

## Tech

* Python - Client side
* Arduino (C) - Hardware Side
* WS2812B LEDs

## Aims

* Allow realtime (60FPS+) screenshots to be taken
* Allow a proportion of the screenshot to be taken and mapped to each individual LED
* Resilience to serial line issues/interruptions
  * Confirm sent vs received
* Allow rendering full frame as fast as possible, or render output on demand
