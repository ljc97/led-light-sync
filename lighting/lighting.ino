#include "FastLED.h"
#define LED_PIN 6
#define LED_TYPE WS2812B
#define LED_ORDER GRB
#define NUM_LEDS 144
#define SERIAL_BAUD 230400
CRGB leds[NUM_LEDS];
void setup()
{
  // put your setup code here, to run once:
  Serial.begin(SERIAL_BAUD);
  FastLED.addLeds<LED_TYPE, LED_PIN, LED_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(50);
  for(int x = 0; x < NUM_LEDS; x++)
  {
    leds[x].setRGB(0,0,0);
  }
  FastLED.show();
}

void loop()
{
  // put your main code here, to run repeatedly:
  if (Serial.available() > 10)
  {
    int x = Serial.parseInt();
    int r = Serial.parseInt();
    int g = Serial.parseInt();
    int b = Serial.parseInt();
    
    Serial.print("RCVD:");
    Serial.print(x);
    Serial.print(";");
    Serial.print(r);
    Serial.print(";");
    Serial.print(g);
    Serial.print(";");
    Serial.print(b);
    Serial.println(":");
    if (x == -1)
    {
      //update LEDs
      FastLED.show();
    }
    else
    {
      leds[x].setRGB(r, g, b);
    }
  }
}
