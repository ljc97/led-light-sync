import serial
import time
import pyscreenshot as ImageGrab
from mss import mss
from PIL import Image
import numpy
import scipy.misc


ser = serial.Serial(port="/dev/ttyACM0",baudrate=230400,timeout=5)
if(ser.isOpen()):
    ser.close()
ser.open()
while not ser.isOpen():
    time.sleep()

# FPS Target Params
delay = 0.0 #Initial Delay
delayIncrement = 0.01 #Amount to increment or decrement delay
averageLength = 15 #Window Size to Consider
fpsTarget = 30 #Target FPS


print("Got Open Serial Port")
ser.readlines()
previousFrame = []

frames = 0
overallStart = time.time()
screenShotCumulative = 0
scalingFactor = 0.02
screenHeight = int(1080 * scalingFactor)
screenWidth = 1920
print("W:" + str(screenWidth) + " H: " + str(screenHeight))



while(True):
    start = time.time()
    # print("Taking Screenshot")
    if(frames % averageLength == 0):
        timedStart = time.time()
    screenShotStart = time.time()

    monitor = {'top': 0, 'left': 0, 'width': screenWidth, 'height': screenHeight}
    with mss() as sct:
        sct.get_pixels(monitor)
        rgb = Image.frombytes('RGB', (sct.width, sct.height), sct.image)
    screenShotEnd = time.time()
    screenShotCumulative += screenShotEnd - screenShotStart
    # print("ScreenFPS: " + str(frames*1.0/screenShotCumulative))

    # print("FirstPixel = " + str(rgb.getpixel((0,0))))
    averagedPixels = []
    rgb = numpy.array(rgb)
    # scipy.misc.toimage(rgb).save("original.png")
    #rgb = 
    #  height * width * 3 channels(RGB)
    rgb = numpy.divide(rgb.sum(0),screenHeight)
    # rgb = 1920 * 3
    for x in range(0,screenWidth):
        averagedPixels.append([int(rgb[x][0]),int(rgb[x][1]),int(rgb[x][2])])
    # scipy.misc.toimage(rgb.reshape(1,1920,3)).save("modified.png")
    # averagedPixels = rgb

    finalPixels = []
    numberOfLEDs = 143
    currentIndex = 0
    sumR = 0
    sumG = 0
    sumB = 0
    count = 0
    for x in range(0,len(averagedPixels)):
        # print(currentIndex)
        # print(round(x*1.0/im.width * numberOfLEDs))
        # print(x)
        if(currentIndex == round(x*1.0/screenWidth * numberOfLEDs)):
            sumR += averagedPixels[x][0]
            sumG += averagedPixels[x][1]
            sumB += averagedPixels[x][2]
            count += 1
        else: 
            #print("Change, Adding")
            finalPixels.append([currentIndex,sumR/count,sumG/count,sumB/count])
            sumR = averagedPixels[x][0]
            sumG = averagedPixels[x][1]
            sumB = averagedPixels[x][2]
            count = 1
            currentIndex += 1
    #print(finalPixels)
    r = 0
    g = 0
    b = 0
    numGood = 0
    numMessages = 0
    for p in finalPixels:
        if(not p in previousFrame):
            numMessages += 1
            data = bytes([p[0],p[1],p[2],p[3]])
            ser.write(data)
            expected = "RCVD:" +str(p[0]) + ";"+str(p[1])+";" +str(p[2]) + ";" + str(p[3]) + ":"
            recieved = ser.readline()
            # recieved = expected
            if(expected in recieved):
                numGood += 1
            else:
                print("Error!")
                print("E: " + expected)
                print("R: " + str(recieved))
    # print("Finished Sending")
    if(numMessages > 0):
        ser.write(bytes([-1,-1,-1,-1]))
        expected = "RCVD:" +str(-1) + ";"+str(-1)+";" +str(-1) + ";" + str(-1) + ":"
        recieved = ser.readline()
        # recieved = expected
        numMessages += 1
        if(expected in recieved):
            numGood += 1
    end = time.time()
    # print("Sent : " + str(numMessages))
    # print("Good : " + str(numGood))
    # print("Took : " + str(end-start))
    # print("Messages/s = " + str(numMessages/(end-start)))
    previousFrame = finalPixels
    overallEnd = time.time()
    frames += 1
    fps = (frames % averageLength)*1.0/(overallEnd-timedStart)
    if(fps > fpsTarget):
        delay += delayIncrement
    else:
        if(delay > 0):
            delay -= delayIncrement
    if(delay > 0):
        time.sleep(delay)
    # print("FPS: " + str((frames % averageLength)*1.0/(overallEnd-timedStart)))
    # print("Delay: " + str(delay))
    print("Overall FPS: " + str((frames)*1.0/(overallEnd-overallStart)))